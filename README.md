# Yacy Helm Chart

* Installs the Search Engine Software [YaCy](https://yacy.net)

## Installing the Chart

To install the chart with the release name `yacy`:

```console
git clone https://gitlab.metager.de/open-source/yacy.git
cd yacy
helm install yacy .
```

## Uninstalling the Chart

To uninstall/delete the yacy deployment:

```console
helm delete yacy
```

To Remove PVC and admin credential secrets
```console
kubectl delete pvc,secret -lappname=yacy
```

The commands remove all the Kubernetes components associated with the chart and deletes the release.

## Admin Credentials

The admin credentials for the web interface get initialized on chart installation with the values under `yacy.admin`. You can and probably should remove the values after installation. If no password is provided a random 10 character string is generated and used. Default username would be admin.

## Configuration

| Parameter                                 | Description                                   | Default                                                 |
|-------------------------------------------|-----------------------------------------------|---------------------------------------------------------|
| `replicaCount`                            | Number of nodes                               | `1`                                                     |
| `yacy.namePrefix`                         | Agent name to be published to other peers     | `nil`                                                   |
| `yacy.port`                               | Node Port to be used for external reachability| `31234`                                                 |
| `yacy.admin.username`                     | Admin Username for web interface              | `admin`                                                 |
| `yacy.admin.realm`                        | Admin Realm                                   | `Yacy Admin UI`                                         |
| `yacy.admin.password`                     | Admin Password                                | `random 10 character string`                            |
| `yacy.volume.class`                       | Storage Class Name for PVC                    | `nil`                                                   |
| `yacy.volume.size`                        | Size of persistent volumes                    | `1Gi`                                                   |
| `yacy.additionalConfig`                   | Yacy Config Values to overwrite               | `nil`                                                   |